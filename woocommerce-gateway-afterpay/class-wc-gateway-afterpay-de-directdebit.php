<?php
/**
 * AfterPay Direct Debit payment method for Germany
 *
 * @category   Class
 * @package    WC_Payment_Gateway
 * @author     arvato Finance B.V.
 * @copyright  since 2011 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * AfterPay Direct Debit payment method for Germany
 *
 * @class       WC_Gateway_Afterpay_De_Directdebit
 * @extends     WC_Gateway_Afterpay_Base_Rest
 * @package     Arvato_AfterPay
 * @author      AfterPay
 */
class WC_Gateway_Afterpay_De_Directdebit extends WC_Gateway_Afterpay_Base_Rest {

	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->id           = 'afterpay_de_directdebit';
		$this->method_title = __( 'AfterPay Germany Direct Debit', 'afterpay-payment-gateway-for-woocommerce' );
		$this->has_fields   = true;

		// Load the form fields.
		$this->init_form_fields();
		$this->show_bankaccount = false;
		$this->order_type       = 'B2C';

		// Load the settings.
		$this->init_settings();

		// Define user set variables for basic settings.
		$this->enabled           = ( isset( $this->settings['enabled'] ) ) ?
			$this->settings['enabled'] : '';
		$this->title             = ( isset( $this->settings['title'] ) ) ?
			$this->settings['title'] : '';
		$this->extra_information = ( isset( $this->settings['extra_information'] ) ) ?
			$this->settings['extra_information'] : '';
		$this->merchantid        = ( isset( $this->settings['merchantid'] ) ) ?
			$this->settings['merchantid'] : '';
		$this->lower_threshold   = ( isset( $this->settings['lower_threshold'] ) ) ?
			$this->settings['lower_threshold'] : '';
		$this->upper_threshold   = ( isset( $this->settings['upper_threshold'] ) ) ?
			$this->settings['upper_threshold'] : '';
		$this->testmode          = ( isset( $this->settings['testmode'] ) ) ?
			$this->settings['testmode'] : '';
		$this->debug_mail        = ( isset( $this->settings['debug_mail'] ) ) ?
			$this->settings['debug_mail'] : '';
		$this->ip_restriction    = ( isset( $this->settings['ip_restriction'] ) ) ?
			$this->settings['ip_restriction'] : '';
		$this->show_advanced     = ( isset( $this->settings['show_advanced'] ) ) ?
			$this->settings['show_advanced'] : 'no';

		// Advanced settings.
		$this->show_phone                            = ( isset( $this->settings['show_phone'] ) ) ?
			$this->settings['show_phone'] : '';
		$this->show_gender                           = ( isset( $this->settings['show_gender'] ) ) ?
			$this->settings['show_gender'] : '';
		$this->show_termsandconditions               = ( isset( $this->settings['show_termsandconditions'] ) ) ?
			$this->settings['show_termsandconditions'] : '';
		$this->exclude_shipping_methods              = ( isset( $this->settings['exclude_shipping_methods'] ) ) ?
			$this->settings['exclude_shipping_methods'] : '';
		$this->use_custom_housenumber_field          = ( isset( $this->settings['use_custom_housenumber_field'] ) ) ?
			$this->settings['use_custom_housenumber_field'] : '';
		$this->use_custom_housenumber_addition_field =
			( isset( $this->settings['use_custom_housenumber_addition_field'] ) ) ?
			$this->settings['use_custom_housenumber_addition_field'] : '';
		$this->compatibility_germanized              = ( isset( $this->settings['compatibility_germanized'] ) ) ?
			$this->settings['compatibility_germanized'] : '';

		// Captures and refunds.
		$this->captures                     = ( isset( $this->settings['captures'] ) ) ?
			$this->settings['captures'] : '';
		$this->captures_way                 = ( isset( $this->settings['captures_way'] ) ) ?
			$this->settings['captures_way'] : '';
		$this->captures_way_based_on_status = ( isset( $this->settings['captures_way_based_on_status'] ) ) ?
			$this->settings['captures_way_based_on_status'] : '';
		$this->refunds                      = ( isset( $this->settings['refunds'] ) ) ?
			$this->settings['refunds'] : '';
		$this->refund_tax_percentage        = ( isset( $this->settings['refund_tax_percentage'] ) ) ?
			$this->settings['refund_tax_percentage'] : '';

		// Client Profile Tracking settings.
		$this->profile_tracking           = ( isset( $this->settings['profile_tracking'] ) ) ?
			$this->settings['profile_tracking'] : '';
		$this->profile_tracking_client_id = ( isset( $this->settings['profile_tracking_client_id'] ) ) ?
			$this->settings['profile_tracking_client_id'] : '';

		// Customer Individual Score.
		$this->customer_individual_score = ( isset( $this->settings['customer_individual_score'] ) ) ?
			$this->settings['customer_individual_score'] : '';

		if ( isset( $this->settings['refunds'] ) && 'yes' === $this->settings['refunds'] ) {
			$this->supports = array( 'refunds' );
		}

		$afterpay_country           = 'DE';
		$afterpay_language          = 'DE';
		$afterpay_currency          = 'EUR';
		$afterpay_invoice_terms     = 'https://documents.myafterpay.com/consumer-terms-conditions/de_de/' . $this->merchantid;
		$afterpay_privacy_statement = 'https://documents.myafterpay.com/privacy-statement/de_de/' . $this->merchantid;
		$afterpay_invoice_icon      = 'https://cdn.myafterpay.com/logo/AfterPay_logo.svg';

		// Apply filters to Country and language.
		$this->afterpay_country           = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language          = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency          = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms     = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->afterpay_privacy_statement = apply_filters( 'afterpay_privacy_statement', $afterpay_privacy_statement );
		$this->icon                       = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );

		// Actions
		/* 2.0.0 */
		add_action(
			'woocommerce_update_options_payment_gateways_' . $this->id,
			array( $this, 'process_admin_options' )
		);

		add_action( 'woocommerce_receipt_afterpay', array( &$this, 'receipt_page' ) );

		// Add event handler for an order Status change.
		add_action( 'woocommerce_order_status_changed', array( $this, 'order_status_change_callback' ), 1000, 4 );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = apply_filters(
			'afterpay_de_directdebit_form_fields', array(
				'enabled'                               => array(
					'title'   => __( 'Enable/Disable', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable AfterPay Germany Direct Debit', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'no',
				),
				'title'                                 => array(
					'title'       => __( 'Title', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => __( 'Lastschrift', 'afterpay-payment-gateway-for-woocommerce' ),
				),
				'extra_information'                     => array(
					'title'       => __( 'Extra information', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'textarea',
					'description' => __( 'Extra information to show to the customer in the checkout', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => 'Zahle bequem per Lastschrifteinzug',
				),
				'api_key'                               => array(
					'title'       => __( 'API Key', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Please enter your AfterPay API Key; this is needed in order to take payment!',
						'afterpay'
					),
					'default'     => '',
				),
				'lower_threshold'                       => array(
					'title'       => __( 'Lower threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '5',
				),
				'upper_threshold'                       => array(
					'title'       => __( 'Upper threshold', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.',
						'afterpay'
					),
					'default'     => '',
				),
				'testmode'                              => array(
					'title'       => __( 'Test Mode', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Enable or disable test mode. The sandbox option is for testing with API keys from developer.afterpay.io.',
						'afterpay'
					),
					'options'     => array(
						'yes'     => 'Yes',
						'sandbox' => 'Yes - Sandbox',
						'no'      => 'No',
					),
				),
				'debug_mail'                            => array(
					'title'       => __( 'Debug mail', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.',
						'afterpay'
					),
					'default'     => '',
				),
				'ip_restriction'                        => array(
					'title'       => __( 'IP restriction', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable',
						'afterpay'
					),
					'default'     => '',
				),
				'show_advanced'                         => array(
					'title'       => __( 'Show advanced settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'select',
					'description' => __(
						'Show advanced settings'
					),
					'options'     => array(
						'yes' => 'Yes',
						'no'  => 'No',
					),
					'default'     => 'no',
				),
				'display_settings'                      => array(
					'title'       => __( 'Display settings', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_phone'                            => array(
					'title'       => __( 'Show phone number', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_gender'                           => array(
					'title'       => __( 'Show gender', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __(
						'Show gender field in AfterPay form in the checkout',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'show_termsandconditions'               => array(
					'title'       => __( 'Show terms and conditions', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'description' => __( 'Show terms and conditions of AfterPay', 'afterpay-payment-gateway-for-woocommerce' ),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'exclude_shipping_methods'              => array(
					'title'       => __( 'Exclude shipping methods', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'multiselect',
					'description' => __( 'Do not show AfterPay if selected shipping methods are used', 'afterpay-payment-gateway-for-woocommerce' ),
					'options'     => $this->get_all_shipping_methods(),
					'default'     => 'yes',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_field'          => array(
					'title'       => __( 'Use custom housenumber field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'use_custom_housenumber_addition_field' => array(
					'title'       => __( 'Use custom housenumber addition field', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'description' => __(
						'Fill in the custom field name used for the housenumber addition, without billing_ or shipping_',
						'afterpay'
					),
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'compatibility_germanized'              => array(
					'title'       => __( 'Compatibility with Germanized', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'checkbox',
					'label'       => __( 'Use the title field from the Germanized plugin', 'afterpay-payment-gateway-for-woocommerce' ),
					'description' => __(
						'This functionality hides the gender field at AfterPay and uses the title field from the Germanized plugin',
						'afterpay'
					),
					'default'     => 'no',
					'class'       => 'afterpay_advanced_setting',
				),
				'merchantid'                            => array(
					'title'       => __( 'Merchant ID', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'text',
					'default'     => '',
					'class'       => 'afterpay_advanced_setting',
					'description' => __('The merchant ID can be used for merchant specific terms and conditions.'),
				),
				'captures_and_refunds_section'          => array(
					'title'       => __( 'Captures and refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '<strong style="color:red">' . __( 'This section contains developer settings that can only be set in contact with a consultant of AfterPay. Please contact AfterPay for more information.' ) . '</strong>',
					'class'       => 'afterpay_advanced_setting',
				),
				'captures'                              => array(
					'title'   => __( 'Enable captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way'                          => array(
					'title'   => __( 'Way of captures', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Way of capturing', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'default' => 'auto_after_authorization',
					'options' => array(
						'auto_after_authorization' => __( 'Automatically after authorization', 'afterpay-payment-gateway-for-woocommerce' ),
						'based_on_status'          => __( 'Based on Woocommerce Status', 'afterpay-payment-gateway-for-woocommerce' ),
					),
					'class'   => 'afterpay_advanced_setting',
				),
				'captures_way_based_on_status'          => array(
					'title'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'label'   => __( 'Status to capture based on', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'select',
					'options' => $this->get_all_possible_order_statuses(),
					'class'   => 'afterpay_advanced_setting',
				),
				'refunds'                               => array(
					'title'   => __( 'Enable refunds', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable refunding', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'yes',
					'class'   => 'afterpay_advanced_setting',
				),
				'refund_tax_percentage'                 => array(
					'title'   => __( 'Refund tax percentage', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'text',
					'label'   => __( 'Default percentage calculated on refunds to AfterPay', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => '0',
					'class'   => 'afterpay_advanced_setting',
				),
				'profile_tracking_section'              => array(
					'title'       => __( 'Profile tracking', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'        => 'title',
					'description' => '',
					'class'       => 'afterpay_advanced_setting',
				),
				'profile_tracking'                      => array(
					'title'   => __( 'Enable profile tracking', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable profile tracking', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'no',
					'class'   => 'afterpay_advanced_setting',
				),
				'profile_tracking_client_id'            => array(
					'title'   => __( 'Client ID for profile tracking', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'text',
					'label'   => __( 'Client ID from AfterPay used for profile tracking', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => '',
					'class'   => 'afterpay_advanced_setting',
				),
				'customer_individual_score'             => array(
					'title'   => __( 'Enable customer individual score', 'afterpay-payment-gateway-for-woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable customer individual score', 'afterpay-payment-gateway-for-woocommerce' ),
					'default' => 'no',
					'class'   => 'afterpay_advanced_setting',
				),
			)
		);
	}

	/**
	 * Payment form on checkout page
	 *
	 * @acces public
	 * @return void
	 */
	public function payment_fields() {
		global $woocommerce;

		if ( 'yes' === $this->profile_tracking ) {
			// Get the woocommerce session for profile tracking.
			$session_id = $woocommerce->session->get_customer_id();
			?>
			<input type="hidden" name="<?php echo esc_attr( $this->id ); ?>_session" value="<?php echo esc_attr( $session_id ); ?>" />
			<script type="text/javascript">
				var _itt = {
				c: '<?php echo esc_attr( $this->profile_tracking_client_id ); ?>',
				s: '<?php echo esc_attr( $session_id ); ?>',
				t: 'CO'};(function() {
				var a = document.createElement('script');
				a.type = 'text/javascript'; a.async = true;
				a.src = '//uc8.tv/7982.js';
				var b = document.getElementsByTagName('script')[0]; b.parentNode.insertBefore(a, b);
				})();
			</script>
			<noscript>
				<img src="//uc8.tv/img/7982/<?php echo esc_attr( $session_id ); ?>" border=0 height=0 width=0 />
			</noscript> 
		<?php } ?>

		<?php if ( 'yes' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( 'sandbox' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST SANDBOX MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

			<?php if ( '' !== $this->extra_information ) : ?>
			<p> <?php echo esc_html( $this->extra_information ); ?></p>
			<?php endif; ?>

			<fieldset>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_bankaccount"><?php echo esc_html_e( 'IBAN account', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_bankaccount" />
			</p>
			<div class="clear"></div>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_bankcode"><?php echo esc_html_e( 'BIC / Swift code', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_bankcode" />
			</p>
			<div class="clear"></div>
			<p class="form-row">
				<?php echo esc_html_e( 'With my order, I authorize Arvato Payment Solutions GmbH, creditor identification number DE23ZZZ00001986600, to collect payments from my account by direct debit. At the same time, I instruct my bank to redeem the direct debits drawn by the payee into my account. Note: I can claim reimbursement of the amount due within eight weeks of the debit date. Applicable in this regard by the contract with my bank conditions.', 'afterpay-payment-gateway-for-woocommerce' ); ?>
			</p>
			<?php if ( 'yes' === $this->show_gender ) : ?>
				<?php if ( 'no' === $this->compatibility_germanized ) : ?>
			<div class="clear"></div>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_gender"><?php echo esc_html_e( 'Gender', 'afterpay-payment-gateway-for-woocommerce' ); ?> <span class="required">*</span></label>
				<span class="gender">
					<select class="gender_select" name="<?php echo esc_attr( $this->id ); ?>_gender">
						<option value="V" selected><?php echo esc_html_e( 'Frau', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="M"><?php echo esc_html_e( 'Herr', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
					</select>
				</span>
			</p>
			<?php endif; ?>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_phone ) : ?>
			<div class="clear"></div>
			<p class="form-row form-row-first validate-required validate-phone">
				<label for="<?php echo esc_attr( $this->id ); ?>_phone"><?php echo esc_html_e( 'Phone number', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_phone" />
			</p>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_termsandconditions ) : ?>
			<div class="clear"></div>
			<p class="form-row validate-required">
				<input type="checkbox" class="input-checkbox" name="<?php echo esc_attr( $this->id ); ?>_terms" /><span class="required">*</span>
				<?php
					echo esc_html__( 'Ich habe die', 'afterpay-payment-gateway-for-woocommerce' ) .
					' <a href="' . esc_url( $this->afterpay_invoice_terms ) .
					'" target="blank">' . esc_html__( 'Allgemeinen Geschäftsbedingungen', 'afterpay-payment-gateway-for-woocommerce' ) .
					'</a> ' .
					esc_html__( 'und die', 'afterpay-payment-gateway-for-woocommerce' ) .
					' <a href="' . esc_url( $this->afterpay_invoice_terms ) .
					'" target="blank">' . esc_html__( 'Datenschutzerklärung', 'afterpay-payment-gateway-for-woocommerce' ) .
					'</a> ' .
					esc_html__( 'von AfterPay gelesen und akzeptiere diese.', 'afterpay-payment-gateway-for-woocommerce' );
				?>
			</p>
			<?php endif; ?>
		</fieldset>
		<?php
	}

	/**
	 * Validate form fields.
	 *
	 * @access public
	 * @return boolean
	 */
	public function validate_fields() {
		global $woocommerce;

		if ( 'yes' === $this->show_phone && ! isset( $_POST[ esc_attr( $this->id ) . '_phone' ] ) ) {
			wc_add_notice( esc_html__( 'Phone number is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'no' === $this->compatibility_germanized && 'yes' === $this->show_gender && ! isset( $_POST[ esc_attr( $this->id ) . '_gender' ] ) ) {
			wc_add_notice( esc_html__( 'Gender is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_termsandconditions && ! isset( $_POST[ esc_attr( $this->id ) . '_terms' ] ) ) {
			wc_add_notice( esc_html__( 'Please accept the AfterPay terms.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( ! isset( $_POST[ esc_attr( $this->id ) . '_bankcode' ] ) ) {
			wc_add_notice( esc_html__( 'BIC / Swift code is a required field.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( ! isset( $_POST[ esc_attr( $this->id ) . '_bankaccount' ] ) ) {
			wc_add_notice( esc_html__( 'IBAN account is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}

		// Validate bank account.
		$afterpay_bankcode    = ( isset( $_POST[ ( $this->id ) . '_bankcode' ] ) ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ ( $this->id ) . '_bankcode' ] ) ) ) : '';
		$afterpay_bankaccount = ( isset( $_POST[ ( $this->id ) . '_bankaccount' ] ) ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ ( $this->id ) . '_bankaccount' ] ) ) ) : '';
		if ( '' !== $afterpay_bankcode && '' !== $afterpay_bankaccount ) {
			if ( ! $this->validate_afterpay_bankaccount( $afterpay_bankcode, $afterpay_bankaccount ) ) {
				return false;
			}
		}
		return true;
	}
}
