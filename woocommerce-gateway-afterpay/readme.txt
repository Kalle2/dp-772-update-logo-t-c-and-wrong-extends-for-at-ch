=== AfterPay payment gateway for Woocommerce ===
Contributors: afterpay
Tags: afterpay, payment, woocommerce
Requires at least: 4.5.0
Requires PHP: 5.6
Tested up to: 5.7
Stable tag: 5.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Author URI: https://developer.afterpay.io

AfterPay is the most consumer-friendly Payment After Delivery method in Germany, Austria, Switzerland, the Nordics, Netherlands and Belgium. Add AfterPay to your WooCommerce storefront with this plugin!

== Description ==

With this official extension, you can add Payment After Delivery methods to your WooCommerce storefront. Your invoices are settled normally by AfterPay, who takes on the credit and fraud risks. Meanwhile, your customers don't have to pay anything until they receive and try the goods - and afterwards, they have a convenient range of options, including flexible installments.

A separate AfterPay account is required to use this extension. Additional fees apply.

How it works:

1) Your customer finds a pair of shoes they like, but are not sure which size fits better.

2) They order two pairs in different sizes, using AfterPay's standard 14-Day Invoice payment method.

3) They receive the shipment, find the size that fits perfectly, and send the other size back.

4) They only pay for the pair they kept - and they have two weeks from receiving the goods to do it!

You can capture and refund AfterPay orders directly from your WooCommerce Admin.

Where it works:

Germany, Austria, Switzerland, Netherlands, Belgium, Denmark, Sweden, Norway, and Finland.

== Installation ==

1. Istall directly through the WordPress Plugins screens or upload the plugin files to the `/wp-content/plugins/plugin-name` directory.
2. Activate in the 'Plugins' screen in WordPress.
3. Go to Woocommerce->Settings->Checkout to configure the AfterPay payment methods.

Contact us at www.afterpay.nl to sign up and get your test and live credentials.

== Changelog ==

2021.* - version 5.4.0

* DP-681 - Add compatibility for NL Shipping methods to REST payment methods.
* DP-761 - Filter active payment methods on active countries.

2021.06.01 - version 5.3.0

* DP-744 - Update PHP Library to version 3.2.0.

2021.05.27 - version 5.2.0

* DP-736 - Allow addresses without housenumbers and remove housenumber and housenumber element when empty.
* DP-747 - Fixed issue with capturing orders because of order fees.
* DP-733 - Fixed issue with streetnames starting with number. 

2021.04.06 - version 5.1.0

* DP-728 - Fixed compatibility issue with MyParcel pickup points.

2021.03.09 - version 5.0.0

* DP-699 - Checked compatibility with Wordpress 5.6 and Woocommerce 5.0
* DP-701 - Fixed compatibility issue with MyParcel pickup points.
* DP-677 - Added AfterPay Strong Customer Authentication.

2020.11.26 - version 4.9.0

* DP-511 - Use the store address when local pickup is used as shipping method.
* DP-454 - Fix deprecated function for canceling orders.
* DP-698 - Updated the AfterPay PHP Library to version 2.9.1

2020.08.13 - version 4.8.0

* DP-663 - Updated and added payment methods for Norway, Sweden, Denmark and Finland.
* DP-662 - Tested compatibility with Wordpress 5.5 and Woocommerce 4.3.2

2020.07.09 - version 4.7.0

* DP-659 - Updated SOAP and REST endpoints.

2020.05.27 - version 4.6.0

* DP-621 - Tested compatibility with Wordpress 5.4.1 and WooCommerce 4.1.0.
* DP-638 - Updated the AfterPay PHP Library to version 2.4.
* DP-644 - Updated the AfterPay PHP Library to version 2.5.
* DP-657 - Updated the AfterPay PHP Library to version 2.6.
* DP-490 - Added French translations for supporting the french speaking part of Belgium.
* DP-295 - Exclude payment method based on shipping method.
* DP-201 - Terms and conditions should always be visible for the Netherlands and Belgium.
* DP-575 - Changed order of bank details fields for DACH.
* DP-545 - Support Dutch 'formal' language.
* DP-544 - Support configured language to used in the authorization for Belgium and Switzerland.
* DP-427 - Added functionality to notify admin about new AfterPay order.
* DP-658 - Add privacy statement text to Danish payment method.

2020.01.08 - version 4.5.0

* DP-468 - Remove pending functionality / push status for Belgium including order update mail
* DP-552 - Hide capture status field when automatisc capturing is selected
* DP-605 - Add warning to not adjust Capture/Refund settings
* DP-221 - Use logo from CDN

2019.11.14 - version 4.4.0

* Tested with Wordpress 5.3 and WooCommerce 3.8.0
* DP-604 - Update to new version of PHP Library 2.2.0
* DP-595 - Add AfterPay logo in SVG format
* DP-607 - Sent in full first name instead of initials for NL and BE requests

2019.05.27 - version 4.3.0

* Fixed issue showing all configuration options at start.
* DP-580 - Add duplicates of payment methods NL open invoice, NL open invoice B2b and DE open invoice for more flexibility.
* Tested with alpha release of Wordpress 5.2.2 and Woocommerce 3.6.3.

2019.01.29 - version 4.2.1

* DP-527 - Updated option for Customer Individual Score to enable and sent default value.

2019.01.29 - version 4.2.0

* DP-532 - Fixed issue with special characters. Description in SOAP is now only allowed on A-Z, a-z, 0-9, space, dash.
* DP-532 - Updated to version 2.0.0 of the AfterPay PHP Library

2019.01.09 - version 4.1.1

* Changed copyright to 2018
* DP-538 - Adjustments for Dutch low vat class.
* DP-518 - Advanced settings should not be shown by default
* DP-541 - Base max order limit on whole cart including shipping and payment fee
* DP-533 - Fixed issue with housenumber addition

2018.12.10 - version 4.1.0

* Tested with alpha release of Wordpress 5.0.1.
* DP-516 - Upgraded to version 1.9 of the AfterPay PHP Library
* DP-493 - Add sandbox as extra webservice to communicate with

2018.11.20 - version 4.0.3

* Tested with beta version of Wordpress 5.0.-beta5

2018.11.07 - version 4.0.2

* DP-504 - Fixed problem activating plugin in Wordpress Multisite Network
* DP-506 - Test compatibility with Woocommerce version 3.5.1

2018.10.18 - version 4.0.1

* DP-481 - Problem with address fields of MyParcel, seperate address fields for shipping were used, causing validation errors.
* DP-240 - Use address fields of pickup point location provided by MyParcel
* DP-329 - Minor translation updates for Norway open invoice and installments

2018.09.27 - version 4.0.0

* DP-449 - Reworked module according to Wordpress Coding Standards (New AfterPay Base and AfterPay Base Rest class where the payment methods are inherited on).

2018.08.10 - version 3.8.0

* Compatible with WP 4.9.8 + Woocommerce 3.4.4
* DP-404 - Added compatibility for PostNL shipment and pickup point delivery address
* DP-384 - Added possibility to show or hide the gender in DACH
* DP-323 - Added profile tracking for DACH
* DP-321 - Added customer individual score for DACH
* DP-386 - Used new terms and conditions and privacy statement from CDN
* DP-141 - Added installments for Sweden
* DP-152 - Added installments for Finland
* DP-148 - Added installments for Norway
* DP-326 - Added 'advanced configuration' to prevent mistakes or misconfigurations
* DP-325 - Used customerfacing message from webservice to show in case of address correction
* Cleaned up duplicate code in classes
* Updated coupon class to WordPress coding standards

2018.06.26 - version 3.7.0

* DP-319 - All javascripts are loaded on all pages, even when AfterPay is not enabled
* DP-311 - Change Payment method description according to display guidelines
* DP-329 - Fixed correct conversation language for Norway, Sweden, Finland
* DP-396 - Default disable captures and refunds for NL and BE
* DP-141 and DP-152 - Added preparations for installments Sweden and Finland
* DP-308 - Enable StreetNumbers with additional letters
* DP-397 - Bug with Sendclound, when no shipping method is used
* DP-308 - Enable StreetNumbers with additional letters
* DP-140 - Add open invoice for Sweden
* DP-398 - Use correct payment terms and conditions for Dutch connections
* DP-399 - Fixed bug with order status when capturing is disabled

2018.05.15 - version 3.6.0

* DP-226 - Added bankaccount validation
* DP-155 - Added direct debit for Germany
* DP-160 - Added direct debit for Austria
* DP-161 - Added direct debit for Switzerland
* DP-257 - Implemented new translations for Denmark
* DP-115 - Added compatibility with Sendcloud
* DP-256 - Woocommerce create option to capture based on status
* Updated to the latest AfterPay Library (1.7.0) + updated dependencies
* DP-269 - Postalcode plugin caused issue not sending shipping housenumber, build in check.

2018.02.12 - version 3.5.1

* DP231 - Add check for product image to be filled with an url, otherwise sent empty string
* DP232 - Add check on vat calculation on product with a price of 0.
* Updated translations

2018.02.07 - version 3.5.0

* Compatible with WP 4.9.4 + Woocommerce 3.3.1
* Updated to the latest AfterPay Library (1.6.0)
* DP-212 - Make address correction also respond on return code 200.101
* DP-135 - Added support for installments in Germany, Austria and Switzerland
* DP-135 - Added new transparent version of the logo
* DP-135 - Added new german translations for installments
* DP-135 - Fixed a bug with multiple items calculating the wrong tax amount for REST calls.
* DP-210 - Implement product images and product urls for OneAPI connections
* DP-140 - Add open invoice for Sweden (still disabled because of translations)
* DP-147 - Add open invoice for Denmark (still disabled because of translations)
* DP-149 - Add open invoice for Norway (still disabled because of translations)
* DP-150 - Add open invoice for Finland (still disabled because of translations)

2017.10.20 - version 3.3.0

* DP-113 - Updated to the latest AfterPay Library (1.4.0)
* DP-109 - Checked compatibility WordPress 4.8.2
* DP-112 - Checked compatibility Woocommerce version 3.2.1
* DP-29 - Fixed IP Restriction Bug
* DP-96 - Added gender as optional field for payment methods NL Open Invoice, NL Direct Debit and BE Open Invoice
* DP-110 - Fields for housenumber and housenumber addition are now configurable

2017.10.12 - version 3.2.0

* Compatible with WP 4.8.2 + Woocommerce 3.1.2
* Update to latest version of AfterPay Library 1.3.0
* Added configuration option to show or hide terms and conditions
* DE: Add a configurable information field to the checkout which can be used for extra information to the customer
* Compatible with Germanized Plugin version (1.9.1), compatible with gender selection and payment fee.
* DE: When an order is rejected the payment will be failed but the order will not be cancelled, to make new payment on the same order possible
* Tested compatibility with Pronamic Pay plugin (4.6.0)
* Placed disclaimer for capture and refund option in the configuration
* Added fallback to addressline 2 when housenumber is used in the second field for address
* Added code cleanup and PHP docblocks for release
* Add 'Call to action' on payment methods to get in contact with the sales department
* Add AfterPay Austria as a payment method
* Add AfterPay Switserland as a payment method
* DE: Sending VAT amounts and percentage in the request
* Updated German translations

2017.10.06 - version 3.1.1

* Hotfix: removed discount rule

2017.09.12 - version 3.1.0

* compatible with WP 4.8.1 + Woocommerce 3.1.2
* Update to latest version of AfterPay Library (1.2.9)
* Added capture and refund functionality to all payment options
* Added small updates and bugfixes to AfterPay Germany as a result of intensive testing
* Show country under address in address correction (AfterPay DE)
* Code cleanup

2017.07.26 - version 3.0.0

* compatible with WP 4.8 + Woocommerce 3.1.1
* Updated code to Woocommerce 3.0 standards, solved deprecation errors
* Fixed bug in overview of payment methods
* Removed merchantid and password check for availability
* Added availability check if not logged in as admin
* Update to latest version of AfterPay Library (1.2.8)
* NL: Check all dutch validation messages in dutch
* Remove text with amount limits in checkout
* Make it possible to restrict on multiple IP addresses (seperated by comma)
* Removed unneccesary javascript, composer files
* Added https to terms and conditions urls

2017.06.19 - version 2.9.1

* Bugfix for problem with showing wordpress menus in combination with this extension

2017.05.10 - version 2.9

* Used new version of the AfterPay Library (1.2.0 https://bitbucket.org/afterpay-plugins/afterpay-composer-package)
* Because of new AfterPay Library names with special characters can be used
* Removed gender from fields
* From this version it is mandatory for customers to agree to the terms and conditions of AfterPay
* compatible with WP 4.7.4 + WooCommerce 3.0.5

2017.01.05 - version 2.8

* The year selectbox of the date of birth field are now starting at the current year minus 18 years and arranged from newest year to oldest year.
* Removed css from checkout and changed field order
* When a validation error occurs, than the order will not be cancelled
* Added new copyright notice
* Updated AfterPay core library to 1.1.8
* compatible with WP 4.7 + WooCommerce 2.6.11

2016.09.05 - version 2.7

* fix for validations in Belgium
* used updated version of composer library

2016.08-26 - version 2.6

* fix for default country, removed option to base housenumber on default country
* fix for calculation of tax rate for shipping based on tax amount

2016.08.09 - version 2.5

* fix for housenumberaddition, now works with space (10 a), nospace (10a), special chars (- + , | )
* fix for vat category when rounding errors
* translation for validation problem with bankaccount numbers

2016.06.13 - version 2.4

* fix for issue where housenumber and housenumberaddition are not send separately
* fix for issue where B2B orders fail due to missing gender field
* removed unneeded B2B fields
* fix for order status not updating on Belgium orders, using WP 4.5.3
* added Dutch translations for all error messages
* compatible with WP 4.5.3 + WooCommerce 2.6.2

2016.04.05 - version 2.3

* Removed automatic update functionality to comply Woocommerce Standards
* Tested compatibility with Woocommerce 2.5.5
* Core AfterPay Class = Composer / Packagist class 1.0.8: https://packagist.org/packages/payintegrator/afterpay
* Changed naming and description of payment methods
* Removed changable option for description
* Removed OsPinto dBug Class
* Added most recent AfterPay Logo

2016.02-09 - version 2.2

* Added AfterPay NL Business 2 Business
* Tested compatibility with Woocommerce 2.4.2
* Several small bugfixes

2015.06.11 - version 2.1

* Added automatic update functionality

2015.06.04 - version 2.0.2

* Added AfterPay Belgium
* Update validation errors
* Added posibility for showing phone number
* Added posibility for ip restriction in testing
* Added better way for requesting client IP address
* Tested compatibility with Woocommerce 2.3.0
* Several small bugfixes

2015.04.22 - version 1.9.1

* Fixed add_error problems
* Fixed several php warning problems

2015.04.13 - version 1.9

* Fixed add_error problems

2015.01.19 - version 1.8

* Added compatibility with http://www.woothemes.com/products/sequential-order-numbers-pro/

2014.10.07 - version 1.7

* Removed surcharge code, advice to use https://wordpress.org/plugins/woocommerce-add-extra-charges-option-to-payment-gateways/
* Fixed WP DEBUG messages

2014.05.06 - version 1.6

* Code cleaned and set to WooCommerce standards. Also added default gateway to extend other payment methods. And payment fee added to cart.

2014.04.30 - version 1.5

* Added AfterPay Direct Debit
* Fixed invoice fee, added as new surcharge method. Now visible in checkout.

2014.04.24 - version 1.4

* Fixed issue with invoice fee, only added with AfterPay orders
* Correct response for validation errors

2014.04.10 - version 1.3

* Added phone number format to AfterPay Library

2014.04.10 - version 1.2

* Fixed return url problem

2014.02.11 - version 1.1

* Removed bankaccount number for SEPA
 
2013.06.19 - version 1.0

* First Release