<?php
/**
 * AfterPay Payment Gateway Base for REST API
 *
 * @category   Class
 * @package    WC_Payment_Gateway
 * @author     arvato Finance B.V.
 * @copyright  since 2011 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * AfterPay Payment Gateway Base for REST API
 *
 * @class       WC_Gateway_Afterpay_De_Openinvoice
 * @extends     WC_Gateway_Afterpay_Base
 * @package     Arvato_AfterPay
 * @author      AfterPay
 */
class WC_Gateway_Afterpay_Base_Rest extends WC_Gateway_Afterpay_Base {

	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;
		parent::__construct();

		// Set defaults.
		$afterpay_language = 'DE';
		$afterpay_currency = 'EUR';

		$this->afterpay_language = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency = apply_filters( 'afterpay_currency', $afterpay_currency );
	}

	/**
	 * Payment form on checkout page
	 *
	 * @acces public
	 * @return void
	 */
	public function payment_fields() {
		global $woocommerce;

		if ( 'yes' === $this->profile_tracking ) {
			// Get the woocommerce session for profile tracking.
			$session_id = $woocommerce->session->get_customer_id();
			?>
			<input type="hidden" name="<?php echo esc_attr( $this->id ); ?>_session" value="<?php echo esc_attr( $session_id ); ?>" />
			<script type="text/javascript">
				var _itt = {
				c: '<?php echo esc_attr( $this->profile_tracking_client_id ); ?>',
				s: '<?php echo esc_attr( $session_id ); ?>',
				t: 'CO'};(function() {
				var a = document.createElement('script');
				a.type = 'text/javascript'; a.async = true;
				a.src = '//uc8.tv/7982.js';
				var b = document.getElementsByTagName('script')[0]; b.parentNode.insertBefore(a, b);
				})();
			</script>
			<noscript>
				<img src="//uc8.tv/img/7982/<?php echo esc_attr( $session_id ); ?>" border=0 height=0 width=0 />
			</noscript> 
		<?php } ?>

		<?php if ( 'yes' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( 'sandbox' === $this->testmode ) : ?>
		<div style="background-color: red; color: white; margin: 10px; padding: 10px; text-align: center; font-weight: bold; text-shadow: none; border-radius: 10px"><?php esc_html_e( 'TEST SANDBOX MODE ENABLED', 'afterpay-payment-gateway-for-woocommerce' ); ?></div>
		<?php endif; ?>

		<?php if ( '' !== $this->extra_information ) : ?>
		<p> <?php echo esc_html( $this->extra_information ); ?></p>
		<?php endif; ?>

		<fieldset>
			<?php if ( 'yes' === $this->show_gender ) : ?>
				<?php if ( 'no' === $this->compatibility_germanized ) : ?>
			<p class="form-row">
				<label for="<?php echo esc_attr( $this->id ); ?>_gender"><?php esc_html_e( 'Gender', 'afterpay-payment-gateway-for-woocommerce' ); ?> <span class="required">*</span></label>
				<span class="gender">
					<select class="gender_select" name="<?php echo esc_attr( $this->id ); ?>_gender">
						<option value="V" selected><?php esc_html_e( 'Frau', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
						<option value="M"><?php esc_html_e( 'Herr', 'afterpay-payment-gateway-for-woocommerce' ); ?></option>
					</select>
				</span>
			</p>
			<div class="clear"></div>
			<?php endif; ?>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_phone ) : ?>
			<div class="clear"></div>
			<p class="form-row form-row-first validate-required validate-phone">
				<label for="<?php echo esc_attr( $this->id ); ?>_phone"><?php esc_html_e( 'Phone number', 'afterpay-payment-gateway-for-woocommerce' ); ?><span class="required">*</span></label>
				<input type="input" class="input-text" name="<?php echo esc_attr( $this->id ); ?>_phone" />    
			</p>
			<?php endif; ?>
			<?php if ( 'yes' === $this->show_termsandconditions ) : ?>
			<div class="clear"></div>
			<p class="form-row validate-required">
				<input type="checkbox" class="input-checkbox" name="<?php echo esc_attr( $this->id ); ?>_terms" /><span class="required">*</span>
				<?php echo esc_html__( 'I accept the', 'afterpay-payment-gateway-for-woocommerce' ) . ' <a href="' . esc_url( $this->afterpay_invoice_terms ) . '" target="blank">' . esc_html__( 'payment terms', 'afterpay-payment-gateway-for-woocommerce' ) . '</a>' . esc_html__( ' from AfterPay.', 'afterpay-payment-gateway-for-woocommerce' ); ?>
			</p>
			<?php endif; ?>
		</fieldset>
		<?php
	}

	/**
	 * Validate form fields.
	 *
	 * @access public
	 * @return boolean
	 */
	public function validate_fields() {
		global $woocommerce;

		if ( 'yes' === $this->show_phone && ! isset( $_POST[ $this->id . '_phone' ] ) ) {
			wc_add_notice( __( 'Phone number is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'no' === $this->compatibility_germanized && 'yes' === $this->show_gender && ! isset( $_POST[ $this->id . '_gender' ] ) ) {
			wc_add_notice( __( 'Gender is a required field', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		if ( 'yes' === $this->show_termsandconditions && ! isset( $_POST[ $this->id . '_terms' ] ) ) {
			wc_add_notice( __( 'Please accept the AfterPay terms.', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
			return false;
		}
		return true;
	}

	/**
	 * Process the payment and return the result
	 *
	 * @access public
	 * @param int $order_id Woocommerce order ID.
	 * @return array
	 **/
	public function process_payment( $order_id ) {
		global $woocommerce;

		$_tax = new WC_Tax();

		$order = wc_get_order( $order_id );

		require_once __DIR__ . '/vendor/autoload.php';

		// Create AfterPay object.
		$afterpay = new \Afterpay\Afterpay();
		$afterpay->setRest();

		// Get values from afterpay form on checkout page.
		// Set form fields per payment option.
		// Collect the dob for NL and BE.
		$afterpay_dob = '';
		if ( in_array( $order->get_billing_country(), array( 'NL', 'BE' ), true ) ) {
			$afterpay_dob_day   = isset( $_POST[ $this->id . '_date_of_birth_day' ] )
				? wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_date_of_birth_day' ] ) ) ) : '';
			$afterpay_dob_month = isset( $_POST[ $this->id . '_date_of_birth_month' ] )
				? wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_date_of_birth_month' ] ) ) ) : '';
			$afterpay_dob_year  = isset( $_POST[ $this->id . '_date_of_birth_year' ] )
				? wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_date_of_birth_year' ] ) ) ) : '';
			$afterpay_dob       = $afterpay_dob_year . '-' . $afterpay_dob_month . '-' . $afterpay_dob_day . 'T00:00:00';
		}

		// Get values from afterpay form on checkout page.
		// Set form fields per payment option.
		$afterpay_phone  = isset( $_POST[ $this->id . '_phone' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_phone' ] ) ) ) : $order->get_billing_phone();
		$afterpay_gender = isset( $_POST[ $this->id . '_gender' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_gender' ] ) ) ) : '';

		// Set POST data for Installments profile and bankdetails.
		$afterpay_installment_profile     = isset( $_POST[ $this->id . '_installmentplan' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_installmentplan' ] ) ) ) : '';
		$afterpay_installment_bankcode    = isset( $_POST[ $this->id . '_bankcode' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_bankcode' ] ) ) ) : '';
		$afterpay_installment_bankaccount = isset( $_POST[ $this->id . '_bankaccount' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_bankaccount' ] ) ) ) : '';

		// Set POST data for Direct Debit bankdetails.
		$afterpay_bankcode    = isset( $_POST[ $this->id . '_bankcode' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_bankcode' ] ) ) ) : '';
		$afterpay_bankaccount = isset( $_POST[ $this->id . '_bankaccount' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_bankaccount' ] ) ) ) : '';

		// Set POST data for profile tracking.
		$profile_tracking_id = isset( $_POST[ $this->id . '_session' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_session' ] ) ) ) : '';

		// Set POST data for social security number.
		$afterpay_ssn = isset( $_POST[ $this->id . '_ssn' ] ) ?
			wc_clean( sanitize_text_field( wp_unslash( $_POST[ $this->id . '_ssn' ] ) ) ) : '';

		// Check if Customer Individual Score is enabled and if so set the code.
		$afterpay_cis_code = '';
		if ( isset($this->customer_individual_score) && 'yes' === $this->customer_individual_score ) {
			// Check if coupons are used, and if it contains AfterPay CIS code.
			$afterpay_cis_code = '20';

			if ( $order->get_used_coupons() && count( $order->get_used_coupons() ) > 0 ) {
				foreach ( $order->get_used_coupons() as $coupon_name ) {

					// Retrieving the coupon ID.
					$coupon_post_obj = get_page_by_title( $coupon_name, OBJECT, 'shop_coupon' );
					$coupon_id       = $coupon_post_obj->ID;

					// Get an instance of WC_Coupon object in an array(necesary to use WC_Coupon methods).
					$coupons_obj = new WC_Coupon( $coupon_id );

					// Afterpay Coupon code.
					if ( $coupons_obj->meta_exists( 'afterpay_cis_code' ) ) {
						$afterpay_cis_code = $coupons_obj->get_meta( 'afterpay_cis_code' );
					}
				}
			}
		}

		// Check if Germanized plugin is used for the gender / title.
		$germanized_billing_title = get_post_meta( $order_id, '_billing_title', true );
		if ( isset($this->compatibility_germanized) && 'yes' === $this->compatibility_germanized && null !== $germanized_billing_title ) {
			switch ( $germanized_billing_title ) {
				case '1':
					$afterpay_gender = 'M';
					break;
				case '2':
					$afterpay_gender = 'V';
					break;
			}
		}

		// Split address into House number and House extension.
		$afterpay_billing_address_1        = $order->get_billing_address_1();
		$afterpay_billing_address_2        = $order->get_billing_address_2();
		$afterpay_billing_address          = $afterpay_billing_address_1 . ' ' . $afterpay_billing_address_2;
		$splitted_address                  = $this->split_afterpay_address( $afterpay_billing_address, true );
		$afterpay_billing_address          = $splitted_address[0];
		$afterpay_billing_house_number     = $splitted_address[1];
		$afterpay_billing_house_extension  = $splitted_address[2];
		$afterpay_shipping_address_1       = $order->get_shipping_address_1();
		$afterpay_shipping_address_2       = $order->get_shipping_address_2();
		$afterpay_shipping_address         = $afterpay_shipping_address_1 . ' ' . $afterpay_shipping_address_2;
		$splitted_address                  = $this->split_afterpay_address( $afterpay_shipping_address, true );
		$afterpay_shipping_address         = $splitted_address[0];
		$afterpay_shipping_house_number    = $splitted_address[1];
		$afterpay_shipping_house_extension = $splitted_address[2];

		// If special field is being used for housenumber then use that field.
		if ( '' !== $this->settings['use_custom_housenumber_field'] ) {
			$afterpay_billing_house_number =
			isset( $_POST[ 'billing_' . $this->settings['use_custom_housenumber_field'] ] )
			? wc_clean( sanitize_text_field( wp_unslash( $_POST[ 'billing_' . $this->settings['use_custom_housenumber_field'] ] ) ) )
			: $afterpay_billing_house_number;

			$afterpay_shipping_house_number =
			isset( $_POST[ 'shipping_' . $this->settings['use_custom_housenumber_field'] ] )
			? wc_clean( sanitize_text_field( wp_unslash( $_POST[ 'shipping_' . $this->settings['use_custom_housenumber_field'] ] ) ) )
			: $afterpay_shipping_house_number;
		}

		// If special field is being used for housenumber addition then use that field.
		if ( '' !== $this->settings['use_custom_housenumber_addition_field'] ) {
			$afterpay_billing_house_extension =
			isset( $_POST[ 'billing_' . $this->settings['use_custom_housenumber_addition_field'] ] )
			? wc_clean( sanitize_text_field( wp_unslash( $_POST[ 'billing_' . $this->settings['use_custom_housenumber_addition_field'] ] ) ) )
			: $afterpay_billing_house_extension;

			$afterpay_shipping_house_extension =
			isset( $_POST[ 'shipping_' . $this->settings['use_custom_housenumber_addition_field'] ] )
			? wc_clean( sanitize_text_field( wp_unslash( $_POST[ 'shipping_' . $this->settings['use_custom_housenumber_addition_field'] ] ) ) )
			: $afterpay_shipping_house_extension;
		}

		// Get connection mode.
		$afterpay_mode = $this->get_connection_mode();

		$authorisation['apiKey'] = $this->settings['api_key'];

		// Create the order.
		$order_items = $this->get_order_items( $order );

		// Cart Contents.
		if ( count( $order_items ) > 0 ) {
			foreach ( $order_items as $item ) {
				$afterpay->create_order_line(
					$item['sku'],
					$item['name'],
					$item['qty'],
					$item['price'],
					$item['tax_category'],
					$item['tax_amount'],
					( isset($item['google_product_category_id']) ? $item['google_product_category_id'] : null ),
					( isset($item['google_product_category']) ? $item['google_product_category'] : null ),
					( isset($item['product_url']) ? $item['product_url'] : null ),
					( isset($item['image_url']) ? $item['image_url'] : null )
				);
			}
		}

		$aporder['billtoaddress']['city']                     = $order->get_billing_city();
		$aporder['billtoaddress']['housenumber']              = $afterpay_billing_house_number;
		$aporder['billtoaddress']['housenumberaddition']      = $afterpay_billing_house_extension;
		$aporder['billtoaddress']['isocountrycode']           = $order->get_billing_country();
		$aporder['billtoaddress']['postalcode']               = $order->get_billing_postcode();
		$aporder['billtoaddress']['referenceperson']['email'] = $order->get_billing_email();
		if ( '' !== $afterpay_gender ) {
			$aporder['billtoaddress']['referenceperson']['gender'] = $afterpay_gender;
		}
		if ( '' !== $afterpay_dob ) {
			$aporder['billtoaddress']['referenceperson']['dob'] = $afterpay_dob;
		}
		$aporder['billtoaddress']['referenceperson']['firstname']   = $order->get_billing_first_name();
		$aporder['billtoaddress']['referenceperson']['isolanguage'] = $this->afterpay_language;
		$aporder['billtoaddress']['referenceperson']['lastname']    = $order->get_billing_last_name();
		$aporder['billtoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
		$aporder['billtoaddress']['streetname']                     = $afterpay_billing_address;

		// Check if social security number is set, if so put it in the billtoaddress.
		if ( '' !== $afterpay_ssn ) {
			$aporder['billtoaddress']['referenceperson']['ssn'] = $afterpay_ssn;
		}

		// Shipping address.
		if ( '' === $order->get_shipping_method() ) {
			// Use billing address if Shipping is disabled in Woocommerce.
			$aporder['shiptoaddress'] = $aporder['billtoaddress'];
		} else {
			$aporder['shiptoaddress']['city']                     = $order->get_shipping_city();
			$aporder['shiptoaddress']['housenumber']              = $afterpay_shipping_house_number;
			$aporder['shiptoaddress']['housenumberaddition']      = $afterpay_shipping_house_extension;
			$aporder['shiptoaddress']['isocountrycode']           = $order->get_shipping_country();
			$aporder['shiptoaddress']['postalcode']               = $order->get_shipping_postcode();
			$aporder['shiptoaddress']['referenceperson']['email'] = $order->get_billing_email();
			if ( '' !== $afterpay_gender ) {
				$aporder['shiptoaddress']['referenceperson']['gender'] = $afterpay_gender;
			}
			if ( '' !== $afterpay_dob ) {
				$aporder['shiptoaddress']['referenceperson']['dob'] = $afterpay_dob;
			}
			$aporder['shiptoaddress']['referenceperson']['firstname']   = $order->get_shipping_first_name();
			$aporder['shiptoaddress']['referenceperson']['isolanguage'] = $this->afterpay_language;
			$aporder['shiptoaddress']['referenceperson']['lastname']    = $order->get_shipping_last_name();
			$aporder['shiptoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
			$aporder['shiptoaddress']['streetname']                     = $afterpay_shipping_address;
		}

		// Check if shipping method 'local_pickup' is used, if so use the location of the store.
		$chosen_shipping_methods = WC()->session->get( 'chosen_shipping_methods' );
		$chosen_shipping_method  = explode( ':', $chosen_shipping_methods[0] );
		$chosen_shipping_method  = $chosen_shipping_method[0];

		if( 'local_pickup' == $chosen_shipping_method ) {

			$aporder['shiptoaddress']['referenceperson']['firstname'] = 'P';
			$aporder['shiptoaddress']['referenceperson']['lastname']  = utf8_decode('Pickup ' . get_bloginfo( 'name' ));

			$store_address_1                                 = get_option( 'woocommerce_store_address' );
			$store_address_2                                 = get_option( 'woocommerce_store_address_2' );
			$store_address                                   = trim($store_address_1 . ' ' . $store_address_2);
			$splitted_address                                = $this->split_afterpay_address( $store_address );
			$afterpay_shipping_address                       = $splitted_address[0];
			$afterpay_shipping_house_number                  = $splitted_address[1];
			$afterpay_shipping_house_extension               = substr( $splitted_address[2], 0, 10 );
			$aporder['shiptoaddress']['streetname']          = utf8_decode( $afterpay_shipping_address );
			$aporder['shiptoaddress']['housenumber']         = utf8_decode( $afterpay_shipping_house_number );
			$aporder['shiptoaddress']['housenumberaddition'] = utf8_decode( $afterpay_shipping_house_extension );

			$store_city                                      = get_option( 'woocommerce_store_city' );
			$aporder['shiptoaddress']['city']                = utf8_decode( $store_city );
			
			$store_postcode                                  = get_option( 'woocommerce_store_postcode' );
			$aporder['shiptoaddress']['postalcode']          = utf8_decode( $store_postcode );

			// The country/state
			$store_raw_country                               = get_option( 'woocommerce_default_country' );

			// Split the country/state
			$split_country                                   = explode( ":", $store_raw_country );

			// Country and state separated:
			$store_country                                   = $split_country[0];
			$aporder['shiptoaddress']['isocountrycode']      = utf8_decode( $store_country );
		}

		// Shipping compatibility checks
		$shipping_compatibility_checked = false;

		// Start compatibility with SendCloud.
		$shipping_items = $order->get_items( 'shipping' );
		$order_shipping = reset( $shipping_items );
		if ( is_object( $order_shipping ) ) {
			$shipping_method = $order_shipping->get_method_id();
			if (
				strpos( $shipping_method, 'service_point_shipping_method' ) !== false
				&& $order->meta_exists( 'sendcloudshipping_service_point_meta' )
			) {
				$sendcloud_meta_data = $order->get_meta( 'sendcloudshipping_service_point_meta' );
				if ( isset( $sendcloud_meta_data['extra'] ) ) {
					$sendcloud_shipping_data                                 = explode( '|', $sendcloud_meta_data['extra'] );
					$sendcloud_shipping_name                                 = isset( $sendcloud_shipping_data[0] ) ? $sendcloud_shipping_data[0] : '';
					$aporder['shiptoaddress']['referenceperson']['initials'] = 'S';
					$aporder['shiptoaddress']['referenceperson']['lastname'] = utf8_decode( $sendcloud_shipping_name );
					$sendcloud_address                                       = $this->split_afterpay_address( $sendcloud_shipping_data[1] );
					$sendcloud_shipping_street                               = isset( $sendcloud_address[0] ) ? $sendcloud_address[0] : '';
					$aporder['shiptoaddress']['streetname']                  = utf8_decode( $sendcloud_shipping_street );
					$sendcloud_shipping_house_number                         = isset( $sendcloud_address[1] ) ? $sendcloud_address[1] : '';
					$aporder['shiptoaddress']['housenumber']                 = $sendcloud_shipping_house_number;
					$sendcloud_shipping_house_extension                      = isset( $sendcloud_address[2] ) ? $sendcloud_address[2] : '';
					$aporder['shiptoaddress']['housenumberaddition']         = utf8_decode( $afterpay_shipping_house_extension );
					$sendcloud_shipping_pcandcity                            = explode( ' ', $sendcloud_shipping_data['2'] );
					$sendcloud_shipping_postalcode                           = isset( $sendcloud_shipping_pcandcity[0] ) ? $sendcloud_shipping_pcandcity[0] : '';
					$aporder['shiptoaddress']['postalcode']                  = utf8_decode( $sendcloud_shipping_postalcode );
					$sendcloud_shipping_city                                 = isset( $sendcloud_shipping_pcandcity[1] ) ? $sendcloud_shipping_pcandcity[1] : '';
					$aporder['shiptoaddress']['city']                        = utf8_decode( $sendcloud_shipping_city );
					$shipping_compatibility_checked = true;
				}
			}
		}
		// End compatibility with SendCloud.

		// Start compatibility with PostNL.
		// Check if the order is sent with postnl.
		if (
			 $order->meta_exists( '_postnl_delivery_options' )
			&& $shipping_compatibility_checked == false
		) {

			// Get the PostNL meta data.
			$postnl_meta_data = $order->get_meta( '_postnl_delivery_options' );
			
			// Check if the pickup points of PostNL are used.
			if ( $postnl_meta_data !== '' ) {
				
			// Check if the pickup points of PostNL are used.
			if ( isset( $postnl_meta_data['location'] ) ) {
				$shipping_compatibility_checked == true;
				$location_name = 'POSTNL ' . $postnl_meta_data['location'];
					$postnl_shipping_street       = isset( $postnl_meta_data['street'] ) ? $postnl_meta_data['street'] : '';
					$postnl_shipping_house_number = isset( $postnl_meta_data['number'] ) ? $postnl_meta_data['number'] : '';
					$postnl_shipping_postalcode   = isset( $postnl_meta_data['postal_code'] ) ? $postnl_meta_data['postal_code'] : '';
					$postnl_shipping_city         = isset( $postnl_meta_data['city'] ) ? $postnl_meta_data['city'] : '';
					$aporder['shiptoaddress']['referenceperson']['initials'] = 'P';
					$aporder['shiptoaddress']['referenceperson']['lastname'] = utf8_decode( $location_name );
					$aporder['shiptoaddress']['streetname']                  = utf8_decode( $postnl_shipping_street );
					$aporder['shiptoaddress']['housenumber']                 = $postnl_shipping_house_number;
					$aporder['shiptoaddress']['postalcode']                  = utf8_decode( $postnl_shipping_postalcode );
					$aporder['shiptoaddress']['city']                        = utf8_decode( $postnl_shipping_city );
				} elseif(@unserialize($postnl_meta_data) !== false) {
					// The serialized data from PostNL is protected, so a ReflectionClass is needed to get the proper data.
					$postnl_meta_data = unserialize($postnl_meta_data);
					$postnl_reflection = new ReflectionClass($postnl_meta_data);
					if($postnl_reflection->hasProperty('pickupLocation')) {
						$postnl_pickup_location = $postnl_reflection->getProperty('pickupLocation');
						$postnl_pickup_location->setAccessible(true);
						$postnl_pickup_location = $postnl_pickup_location->getValue($postnl_meta_data);
						$postnl_pickup_reflection = new ReflectionClass($postnl_pickup_location);
						if($postnl_pickup_reflection->hasProperty('location_name')) {
							$postnl_location_name = $postnl_pickup_reflection->getProperty('location_name');
							$postnl_location_name->setAccessible(true);
							$location_name = 'POSTNL ' . $postnl_location_name->getValue( $postnl_pickup_location );
							$aporder['shiptoaddress']['referenceperson']['initials'] = 'P';
							$aporder['shiptoaddress']['referenceperson']['lastname'] = utf8_decode( $location_name );
						}
						if($postnl_pickup_reflection->hasProperty('street')) {
							$postnl_street = $postnl_pickup_reflection->getProperty('street');
							$postnl_street->setAccessible(true);
							$aporder['shiptoaddress']['streetname'] = utf8_decode( $postnl_street->getValue( $postnl_pickup_location ) );
						}
						if($postnl_pickup_reflection->hasProperty('number')) {
							$postnl_number = $postnl_pickup_reflection->getProperty('number');
							$postnl_number->setAccessible(true);
							$aporder['shiptoaddress']['housenumber'] = $postnl_number->getValue( $postnl_pickup_location );
						}
						if($postnl_pickup_reflection->hasProperty('postal_code')) {
							$postnl_postal_code = $postnl_pickup_reflection->getProperty('postal_code');
							$postnl_postal_code->setAccessible(true);
							$aporder['shiptoaddress']['postalcode'] = utf8_decode( $postnl_postal_code->getValue( $postnl_pickup_location ) );
						}
						if($postnl_pickup_reflection->hasProperty('city')) {
							$postnl_city = $postnl_pickup_reflection->getProperty('city');
							$postnl_city->setAccessible(true);
							$aporder['shiptoaddress']['city'] = utf8_decode( $postnl_city->getValue( $postnl_pickup_location ) );
						}
					}
				}
			}
		}
		// End compatibility with PostNL.

		// Start compatibility with MyParcel.
		// If PostNL Pickup points are used, use location from pickup point as address data.
		if (
			$order->meta_exists( '_myparcel_delivery_options' )
			&& $shipping_compatibility_checked == false
		) {
			// Get the MyParcel meta data.
			$mpc_meta_delivery_options = $order->get_meta('_myparcel_delivery_options');
			if($mpc_meta_delivery_options !== null) {
				$mpc_meta_data = json_decode($mpc_meta_delivery_options);
				if($mpc_meta_data->isPickup === true) {
					$mpc_pickup_name       = isset( $mpc_meta_data->pickupLocation->location_name ) ? $mpc_meta_data->pickupLocation->location_name : '';
					$mpc_pickup_street     = isset( $mpc_meta_data->pickupLocation->street ) ? $mpc_meta_data->pickupLocation->street : '';
					$mpc_pickup_number     = isset( $mpc_meta_data->pickupLocation->number ) ? $mpc_meta_data->pickupLocation->number : '';
					$mpc_pickup_postalcode = isset( $mpc_meta_data->pickupLocation->postal_code ) ? $mpc_meta_data->pickupLocation->postal_code : '';
					$mpc_pickup_city       = isset( $mpc_meta_data->pickupLocation->city ) ? $mpc_meta_data->pickupLocation->city : '';
					$aporder['shiptoaddress']['referenceperson']['initials'] = 'P';
					$aporder['shiptoaddress']['referenceperson']['lastname'] = utf8_decode( $mpc_pickup_name );
					$aporder['shiptoaddress']['streetname'] = utf8_decode( $mpc_pickup_street );
					$aporder['shiptoaddress']['housenumber'] = utf8_decode( $mpc_pickup_number );
					$aporder['shiptoaddress']['postalcode'] = utf8_decode( $mpc_pickup_postalcode );
					$aporder['shiptoaddress']['city'] = utf8_decode( $mpc_pickup_city );
				}
			}
		}
		// End compatibility with MyParcel.

		// Check if housenumber field is filled for shipping, else use housenumber of billing.
		if (
			'' === $aporder['shiptoaddress']['housenumber']
		) {
			$aporder['shiptoaddress']['housenumber']         = $aporder['billtoaddress']['housenumber'];
			$aporder['shiptoaddress']['housenumberaddition'] = $aporder['billtoaddress']['housenumberaddition'];
		}

		$aporder['ordernumber'] = filter_var( $order->get_order_number(), FILTER_SANITIZE_NUMBER_INT );
		$aporder['currency']    = $this->afterpay_currency;
		$aporder['ipaddress']   = $this->get_afterpay_client_ip();

		// Check if direct debit data is set, if so add to the $aporder array.
		if ( '' !== $afterpay_bankcode ) {
			$aporder['directDebit']['bankCode']    = $afterpay_bankcode;
			$aporder['directDebit']['bankAccount'] = $afterpay_bankaccount;
		}

		// Check if installment data is set, if so add to the $aporder array.
		if ( '' !== $afterpay_installment_profile ) {
			$aporder['installment']['profileNo']   = $afterpay_installment_profile;
			$aporder['installment']['bankCode']    = $afterpay_installment_bankcode;
			$aporder['installment']['bankAccount'] = $afterpay_installment_bankaccount;

			// Unset direct debit data.
			unset( $aporder['directDebit'] );
		}

		// Check if profile tracking data is set, if so add to the $aporder array.
		if ( '' !== $profile_tracking_id ) {
			$aporder['profileTrackingId'] = $profile_tracking_id;
		}

		// Check if customerIndividualScore is set, if so add to the $aporder array.
		if ( '' !== $afterpay_cis_code ) {
			$aporder['customerIndividualScore'] = $afterpay_cis_code;
		}

		// Chef if the Merchant ID is set, if so add to the $aporder array.
		if ( '' !== $this->merchantid ) {
			$aporder['apiMerchantId'] = $this->merchantid;
		}

		try {
			// Transmit all the specified data, from the steps above, to afterpay.
			$afterpay->set_order( $aporder, $this->order_type );
			$afterpay->do_request( $authorisation, $afterpay_mode );
			$this->send_afterpay_debug_mail( $afterpay );

			// Retreive response.
			if ( isset( $afterpay->order_result->return->statusCode ) ) {
				switch ( $afterpay->order_result->return->statusCode ) {
					case 'A':
						// If capturing is enabled, and way of capture is set.
						// to automatically after authorization, then capture the full order.
						if (
							isset( $this->settings['captures'] )
							&& 'yes' === $this->settings['captures']
							&& isset( $this->settings['captures_way'] )
							&& 'auto_after_authorization' === $this->settings['captures_way']
						) {
							$order->add_order_note( __( 'AfterPay payment completed.', 'afterpay-payment-gateway-for-woocommerce' ) );

							// Capture payment.
							$this->capture_afterpay_payment( null, $order );
						}

						if (
							isset( $this->settings['captures'] )
							&& 'yes' === $this->settings['captures']
							&& isset( $this->settings['captures_way'] )
							&& 'auto_after_authorization' !== $this->settings['captures_way']
						) {
							// Add note that the order is not captured yet.
							$order->add_order_note( __( 'AfterPay capture needed, since the Capture mode was set to(Based on Woocommerce Status) when the order was placed.', 'afterpay-payment-gateway-for-woocommerce' ) );
						}

						// Payment complete.
						$order->payment_complete();

						// Remove cart.
						$woocommerce->cart->empty_cart();

						// Return thank you redirect.
						return array(
							'result'   => 'success',
							'redirect' => $this->get_return_url( $order ),
						);
					case 'P':
						$order->add_order_note( __( 'AfterPay payment pending.', 'afterpay' ) );

						// Payment to pending.
						$order->update_status( 'pending', __( 'Awaiting AfterPay payment', 'afterpay' ) );

						// If secureLoginUrl is provided, then redirect.
						if(
							isset( $afterpay->order_result->return->secureLoginUrl )
							&& '' !== $afterpay->order_result->return->secureLoginUrl
						) {
							$redirectUrl = $afterpay->order_result->return->secureLoginUrl;
							$redirectUrl .= site_url('afterpay/return?order_id=' . $order->get_id() );

							return array(
								'result'   => 'success',
								'redirect' => $redirectUrl
							);
						} else {
							// Remove cart.
							$woocommerce->cart->empty_cart();

							// Cancel order to make new order possible.
							WC()->session->set( 'order_awaiting_payment', false );
							$order->update_status( 'cancelled', '' );

							return;
						}
					case 'W':
						// Order is denied, store it in a database.
						$order->add_order_note( __( 'AfterPay payment denied.', 'afterpay-payment-gateway-for-woocommerce' ) );
						$message = isset( $afterpay->order_result->return->messages->message ) ?
							$afterpay->order_result->return->messages->message : '';

						// Check if description failure isset, else set rejectCode to return default rejection message.
						if ( isset( $afterpay->order_result->return->riskCheckMessages )
							&& isset( $afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage )
							&& isset( $afterpay->order_result->return->riskCheckMessages[0]->code ) ) {
							// Get the rejection message from REST.
							$message = $afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage;

							// If the rejection is an address correction, show the address correction.
							if (
								in_array(
									$afterpay->order_result->return->riskCheckMessages[0]->code,
									array( '200.101', '200.103', '200.104' ),
									true
								)
								&& isset( $afterpay->order_result->return->customer->addressList[0] )
								&& is_object( $afterpay->order_result->return->customer->addressList[0] ) ) {
									$new_address = $afterpay->order_result->return->customer->addressList[0];
									$message     = $afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage;
									$message    .= '<br/><br/>';
									$message    .= $new_address->street . ' ' . $new_address->streetNumber . '<br/>';
									$message    .= $new_address->postalCode . ' ';
									$message    .= $new_address->postalPlace . '<br/>';
									$message    .= $new_address->countryCode;
							}
						}
						$order->add_order_note( $message );
						wc_add_notice( $message, 'error' );

						// Cancel order to make new order possible.
						WC()->session->set( 'order_awaiting_payment', false );
						$order->update_status( 'cancelled', '' );

						return;
				}
			} else {

				// Check for business errors.
				if ( 1 === $afterpay->order_result->return->resultId ) {
					// Unknown response, store it in a database.
					$order->add_order_note( __( 'There is a problem with submitting this order to AfterPay.', 'afterpay-payment-gateway-for-woocommerce' ) );
					$validationmsg  = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay-payment-gateway-for-woocommerce' );
					$validationmsg .= '<ul>';
					if ( isset( $afterpay->order_result->return->messages )
						&& ! is_object( $afterpay->order_result->return->messages ) ) {
						foreach ( $afterpay->order_result->return->messages as $value ) {
							$validationmsg .= '<li style="list-style: inherit">' . __( $value->description, 'afterpay-payment-gateway-for-woocommerce' ) . '</li>';
							$order->add_order_note( __( $value->description, 'afterpay-payment-gateway-for-woocommerce' ) );
						}
					} elseif ( isset( $afterpay->order_result->return->failures->failure ) ) {
						$validationmsg .= '<li style="list-style: inherit">' .
							__( $afterpay->order_result->return->failures->failure, 'afterpay-payment-gateway-for-woocommerce' ) . '</li>';
					}
					$validationmsg .= '</ul>';
					wc_add_notice( $validationmsg, 'error' );
				} elseif ( 2 === $afterpay->order_result->return->resultId ) {
					// Unknown response, store it in a database.
					$order->add_order_note( __( 'There is a problem with submitting this order to AfterPay.', 'afterpay-payment-gateway-for-woocommerce' ) );
					$validationmsg  = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay-payment-gateway-for-woocommerce' );
					$validationmsg .= '<ul>';
					if ( ! is_object( $afterpay->order_result->return->messages ) ) {
						foreach ( $afterpay->order_result->return->messages as $value ) {
							$validationmsg .= '<li style="list-style: inherit">' . __( $value->description, 'afterpay-payment-gateway-for-woocommerce' ) . '</li>';
							$order->add_order_note( __( $value->description, 'afterpay-payment-gateway-for-woocommerce' ) );
						}
					}
					$validationmsg .= '</ul>';
					wc_add_notice( $validationmsg, 'error' );
				} else {
					// Unknown response, store it in a database.
					$order->add_order_note( __( 'Unknown response from AfterPay.', 'afterpay-payment-gateway-for-woocommerce' ) );
					wc_add_notice( __( 'Unknown response from AfterPay. Please contact our customer service', 'afterpay-payment-gateway-for-woocommerce' ), 'error' );
				}

				// Cancel order to make new order possible.
				WC()->session->set( 'order_awaiting_payment', false );
				$order->update_status( 'cancelled', '' );

				return;
			}
		} catch ( Exception $e ) {
			// The purchase was denied or something went wrong, print the message.
			// translators: %1$s: error message, %2$s: error code.
			wc_add_notice( sprintf( __( '%1$s (Error code: %2$s)', 'afterpay-payment-gateway-for-woocommerce' ), $e->getMessage(), $e->getCode() ), 'error' );
			return;
		}
	}

	/**
	 * Captures a payment for an order that has not yet been captured
	 *
	 * @param int      $id    The order ID.
	 * @param resource $order The order object itself.
	 */
	public function capture_afterpay_payment( $id, $order ) {
		require_once __DIR__ . '/vendor/autoload.php';

		try {
			// Get connection mode.
			$afterpay_mode = $this->get_connection_mode();

			// API Key.
			$authorisation['apiKey'] = $this->settings['api_key'];

			$afterpay_capture = new \Afterpay\Afterpay();
			$afterpay_capture->setRest();
			$afterpay_capture->set_ordermanagement( 'capture_full' );

			// Set up the additional information.
			$capture_details['invoicenumber'] = filter_var(
				$order->get_order_number(),
				FILTER_SANITIZE_NUMBER_INT
			);
			$capture_details['ordernumber']   = filter_var(
				$order->get_order_number(),
				FILTER_SANITIZE_NUMBER_INT
			);

			// Create the order.
			$order_items = $this->get_order_items( $order );

			// Cart Contents.
			if ( count( $order_items ) > 0 ) {
				foreach ( $order_items as $item ) {
					$afterpay_capture->create_order_line(
						$item['sku'],
						$item['name'],
						$item['qty'],
						$item['price'],
						$item['tax_category'],
						$item['tax_amount'],
						( isset($item['google_product_category_id']) ? $item['google_product_category_id'] : null ),
						( isset($item['google_product_category']) ? $item['google_product_category'] : null ),
						( isset($item['product_url']) ? $item['product_url'] : null ),
						( isset($item['image_url']) ? $item['image_url'] : null )
					);
				}
			}

			// Add order total in cents.
			$capture_details['totalamount']    = $order->get_total() * 100;
			$capture_details['totalNetAmount'] = ( $order->get_total() - $order->get_total_tax() );

			// Create the order object for order management (OM).
			$afterpay_capture->set_order( $capture_details, 'OM' );
			$afterpay_capture->do_request( $authorisation, $afterpay_mode );

			// Send the debug mail.
			$this->send_afterpay_debug_mail( $afterpay_capture );

			if ( isset( $afterpay_capture->order_result->return->resultId ) ) {
				if ( 0 === $afterpay_capture->order_result->return->resultId ) {

					// Payment complete.
					$order->payment_complete();

					$order->add_order_note( __( 'AfterPay payment completed.', 'afterpay-payment-gateway-for-woocommerce' ) );
					$order->add_order_note( __( 'AfterPay payment captured.', 'afterpay-payment-gateway-for-woocommerce' ) );
				} else {
					$order->add_order_note( __( 'Problem with capturing order.', 'afterpay-payment-gateway-for-woocommerce' ) );
				}
			}
		} catch ( Exception $e ) {
			// The purchase was denied or something went wrong, print the message.
			// translators: %1$s: error message, %2$s: error code.
			wc_add_notice( sprintf( __( '%1$s (Error code: %2$s)', 'afterpay-payment-gateway-for-woocommerce' ), $e->getMessage(), $e->getCode() ), 'error' );
			return;
		}
	}

	/**
	 * Get all the order items including shipment and fees and put them in an array to process.
	 *
	 * @param resource $order The order object itself.
	 * @return array
	 */
	private function get_order_items( $order ) {

		global $woocommerce;

		$order_lines = array();

		// Iterate through the order items.
		if ( count( $order->get_items() ) > 0 ) {
			foreach ( $order->get_items() as $item ) {
				// Get product to retrieve sku or product id.
				$_product = $item->get_product();
				// Get SKU or product id.
				if ( $_product->get_sku() ) {
					$sku = $_product->get_sku();
				} else {
					$sku = $_product->get_id();
				}
				$item_tax_amount = round( $order->get_line_tax( $item ) / $item['qty'], 4 );

				// Apply_filters to item price so we can filter this if needed.
				$afterpay_item_price_including_tax = $order->get_item_total( $item, true );
				$item_price                        = apply_filters( 'afterpay_item_price_including_tax', $afterpay_item_price_including_tax );
				$item_price                        = round( $item_price * 100, 0 );

				// Get the product url.
				$product_url = get_permalink( $_product->get_id() );

				// Get the product image url, if not available return empty string.
				$image_url = wp_get_attachment_image_url( $_product->get_image_id(), 'full' );
				if ( false === $image_url ) {
					$image_url = '';
				}

				$order_lines[] = array(
					'sku'                        => $sku,
					'name'                       => $item['name'],
					'qty'                        => $item['qty'],
					'price'                      => $item_price,
					'tax_category'               => null,
					'tax_amount'                 => $item_tax_amount,
					'google_product_category_id' => null, // $googleProductCategoryId
					'google_product_category'    => null, // $googleProductCategory
					'product_url'                => $product_url,
					'image_url'                  => $image_url,
				);
			}
		}

		// Shipping.
		if ( $order->get_shipping_total() > 0 ) {
			// We manually calculate the shipping tax percentage here.
			$calculated_shipping_tax_percentage = ( $order->get_shipping_tax() / $order->get_shipping_total() ) * 100;
			$calculated_shipping_tax_decimal    = ( $order->get_shipping_tax() / $order->get_shipping_total() ) + 1;

			// Apply_filters to Shipping so we can filter this if needed.
			$afterpay_shipping_price_including_tax = $order->get_shipping_total() * $calculated_shipping_tax_decimal;
			$shipping_price                        = apply_filters( 'afterpay_shipping_price_including_tax', $afterpay_shipping_price_including_tax );
			$shipping_sku                          = __( 'Shipping', 'afterpay-payment-gateway-for-woocommerce' );
			$shipping_description                  = __( 'Shipping on order', 'afterpay-payment-gateway-for-woocommerce' );
			$shipping_price                        = round( $shipping_price * 100, 0 );
			$shipping_tax                          = $order->get_shipping_tax();
			$order_lines[]                         = array(
				'sku'          => $shipping_sku,
				'name'         => $shipping_description,
				'qty'          => 1,
				'price'        => $shipping_price,
				'tax_category' => null,
				'tax_amount'   => $shipping_tax,
			);
		}

		// Check if any fees are set on the order.
		$fees = [];
		if(method_exists($woocommerce->cart, 'get_fees')) {
			$fees = $woocommerce->cart->get_fees();
		}

		if ( count( $fees ) > 0 ) {
			foreach ( $fees as $fee ) {
				if(
					!isset($fee->name)
					&& !isset($fee->amount)
					&& !isset($fee->tax)
				) {
					next;
				}
				$fee_sku         = __( 'Service Fee', 'afterpay-payment-gateway-for-woocommerce' );
				$fee_description = $fee->name;
				$fee_price       = round( ( $fee->amount + $fee->tax ) * 100 );
				$order_lines[]   = array(
					'sku'          => $fee_sku,
					'name'         => $fee_description,
					'qty'          => 1,
					'price'        => $fee_price,
					'tax_category' => null,
					'tax_amount'   => $fee->tax,
				);
			}
		}

		return $order_lines;
	}

	/**
	 * Process refunds.
	 * WooCommerce 2.2 or later.
	 *
	 * @param  int    $order_id Woocommerce Order Id.
	 * @param  float  $amount   Refund amount.
	 * @param  string $reason   Optional refund reason.
	 * @return bool|WP_Error
	 */
	public function process_refund( $order_id, $amount = null, $reason = '' ) {

		try {

			global $woocommerce;
			$order = wc_get_order( $order_id );

			// Load AfterPay Library.
			require_once __DIR__ . '/vendor/autoload.php';

			// Create AfterPay object.
			$afterpay = new \Afterpay\Afterpay();
			$afterpay->setRest();

			// Set order management action to partial refund.
			$afterpay->set_ordermanagement( 'refund_partial' );

			// Check the refund id.
			if ( metadata_exists( 'post', $order_id, '_afterpay_refund_id' ) ) {
				$refund_id = get_post_meta( $order_id, '_afterpay_refund_id', true ) + 1;
			} else {
				$refund_id = 1;
			}

			// Set up the additional information.
			$aporder['invoicenumber']       = filter_var(
				$order->get_order_number(),
				FILTER_SANITIZE_NUMBER_INT
			);
			$aporder['ordernumber']         = filter_var(
				$order->get_order_number(),
				FILTER_SANITIZE_NUMBER_INT
			);
			$aporder['creditinvoicenumber'] = 'REFUND-' . filter_var(
				$order->get_order_number(),
				FILTER_SANITIZE_NUMBER_INT
			) . '-' . $refund_id;

			// Set the country.
			$aporder['billtoaddress']['isocountrycode'] = $this->afterpay_country;

			// Set refund line.
			$sku  = 'REFUND';
			$name = 'REFUND';

			// If a reason has been set, use it in  the name/description.
			if ( '' !== $reason ) {
				$name = $name . ': ' . $reason;
			}
			$qty            = 1;
			$price          = round( $amount * 100, 0 ) * -1;
			$tax_category   = 1; // 1 = high, 2 = low, 3, zero, 4 no tax.
			$tax_percentage = $this->refund_tax_percentage;
			$tax_amount     = $this->calculate_afterpay_vat_amount( $amount, $tax_percentage ) * -1;
			$afterpay->create_order_line(
				$sku,
				$name,
				$qty,
				$price,
				$tax_category,
				$tax_amount
			);

			// Create the order object for order management (OM).
			$afterpay->set_order( $aporder, 'OM' );

			// Get connection mode.
			$afterpay_mode = $this->get_connection_mode();

			// Set up the AfterPay credentials and sent the request.
			$authorisation['apiKey'] = $this->settings['api_key'];

			$afterpay->do_request( $authorisation, $afterpay_mode );

			$this->send_afterpay_debug_mail( $afterpay );

			if ( 0 === $afterpay->order_result->return->resultId ) {
				if ( 1 === $refund_id ) {
					add_post_meta( $order_id, '_afterpay_refund_id', 1, true );
				} else {
					update_post_meta( $order_id, '_afterpay_refund_id', $refund_id );
				}
				return true;
			} else {
				return new WP_Error( 'afterpay_refund_error', $afterpay->order_result->return->messages[0]->description );
			}
		} catch ( Exception $e ) {
			return new WP_Error( 'afterpay_refund_error', $e->getMessage() );
		}
		return false;
	}

	/**
	 * Check order status.
	 *
	 * @param  object $order    Woocommerce Order Object.
	 * @return string $status   Order status
	 */
	public function afterpay_check_status( $order ) {

		// Load AfterPay Library.
		require_once __DIR__ . '/vendor/autoload.php';

		// Create AfterPay object.
		$afterpay = new \Afterpay\Afterpay();
		$afterpay->setRest();

		// Set order management action to get the order
		$afterpay->set_ordermanagement( 'get_order' );

		// Set up the additional information
		$aporder['ordernumber'] = filter_var(
			$order->get_order_number(),
			FILTER_SANITIZE_NUMBER_INT
		);

		// Create the order object for B2C or B2B
		$afterpay->set_order( $aporder, 'OM' );

		// Get connection mode.
		$afterpay_mode = $this->get_connection_mode();

		// Set up the AfterPay credentials and sent the request.
		$authorisation['apiKey'] = $this->settings['api_key'];

		$afterpay->do_request( $authorisation, $afterpay_mode );

		$return = $afterpay->order_result->return->orderDetails->status;

		return $return;
	}
}